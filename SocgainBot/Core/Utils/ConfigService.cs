﻿using System;
using System.Linq;
using LiteDB;

namespace Program.Core.Utils
{
    public class ConfigService
    {
        private LiteDatabase _db;
        private static ConfigService _instance;
        
        public static ConfigService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ConfigService();
                }

                return _instance;
            }
        }

        private class ConfigEntry
        {
            public Guid Id { get; set; }
            public string Key { get; set; }
            public object Value { get; set; }
        }

        private ConfigService()
        {
            _db = new LiteDatabase(@"conf.db");
        }
        
        public object this[string key] 
        {
            get => GetValue<object>(key);

            set => SetValue(key, value);
        }  
        
        public T GetValue<T>(string key)
        {
            var collection = _db.GetCollection<ConfigEntry>("config");

            return (T) collection.FindOne(x => x.Key == key).Value;
        }

        public void SetValue<T>(string key, T value)
        {
            var collection = _db.GetCollection<ConfigEntry>("config");

            try
            {
                var data = collection.FindOne(x => x.Key == key);
                data.Value = value;
                
                collection.Update(data);
            }
            catch (Exception e)
            {
                collection.Insert(new ConfigEntry() {Key = key, Value = value});
            }
        }

        public void DeleteValue(string key)
        {
            var collection = _db.GetCollection<ConfigEntry>("config");

            collection.Delete(x => x.Key == key);
        }
    }
}