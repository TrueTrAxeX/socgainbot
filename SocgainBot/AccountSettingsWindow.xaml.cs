﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using SocgainBot.Core.Database;
using SocgainBot.Core.Database.Model;

namespace SocgainBot
{
    public partial class AccountSettingsWindow
    {
        private void LoadUiData(UserAccount acc)
        {
            DelayAfterLikeTask.Text = acc.DelayAfterLikeTask.ToString();
            DelayAfterSubscribeTask.Text = acc.DelayAfterSubscribeTask.ToString();
            
            UserAgent.Text = acc.BrowserUserAgent;

            if (acc.ProxyData != null)
            {
                ProxyAddress.Text = acc.ProxyData.Address.ToString();
                
                if(!string.IsNullOrEmpty(acc.Login))
                    ProxyLogin.Text = acc.ProxyData.Login;
                
                if(!string.IsNullOrEmpty(acc.Password))
                    ProxyPassword.Text = acc.ProxyData.Password;
            }

        }
        
        public AccountSettingsWindow(UserAccount account)
        {
            InitializeComponent();

            var acc = DatabaseManager.UserAccountCollection.FindOne(x => x.Id.Equals(account.Id));

            if (acc == null)
            {
                MessageBox.Show("Аккаунт не найден! Попробуйте перезайти в программу");
                
                return;
            }
            
            LoadUiData(acc);
            
            WatchPassword.Click += (sender, args) =>
            {
                Clipboard.SetText(acc.Password);
                MessageBox.Show($"Пароль от аккаунта: {acc.Password}\n\nПароль скопирован в буфер обмена (Ctrl+C)", "Информация");
            };
            
            SaveBtn.Click += (sender, args) => {
                try
                {
                    acc.DelayAfterLikeTask = int.Parse(DelayAfterLikeTask.Text);
                    acc.DelayAfterSubscribeTask = int.Parse(DelayAfterSubscribeTask.Text);

                    if (string.IsNullOrEmpty(UserAgent.Text))
                    {
                        acc.BrowserUserAgent = null;
                    }
                    else
                    {
                        acc.BrowserUserAgent = UserAgent.Text;
                    }

                    if (!string.IsNullOrEmpty(ProxyAddress.Text))
                    {
                        acc.ProxyData = new ProxyData()
                        {
                            Address = new Uri(ProxyAddress.Text),
                        };

                        if (!string.IsNullOrEmpty(ProxyLogin.Text) && !string.IsNullOrEmpty(ProxyPassword.Text))
                        {
                            acc.ProxyData.Login = ProxyLogin.Text;
                            acc.ProxyData.Password = ProxyPassword.Text;
                        }
                    }
                    else
                    {
                        acc.ProxyData = null;
                    }

                    DatabaseManager.UserAccountCollection.Update(acc);

                    MessageBox.Show("Данные сохранены", "Успех", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Ошибка сохранения! Введены не верные данные.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                } 
            };
        }
    }
}
