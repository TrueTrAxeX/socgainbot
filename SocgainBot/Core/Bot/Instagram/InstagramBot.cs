﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SocgainBot.Core.Database.Model;
using Cookie = System.Net.Cookie;

namespace SocgainBot.Core.Bot.Instagram
{
    public class InstagramBot
    {
        public bool IsAuthenticated { get; set; }
        
        private RestClient _restClient;
        
        public RestClient RestClient
        {
            get { return _restClient; }
        }

        public class InstagramCredentials
        {
            public string Login { get; set; }
            public string Password { get; set; }
        }

        public enum SubscribeResult
        {
            Subscribed, Unsubscribed, Error, BlockedByInstagram
        }

        public enum LikeResult
        {
            Liked, Unliked, Error, BlockedByInstagram
        }

        public bool IsAuth()
        {
            return true;
        }
        
        private static readonly Random _rand = new Random();

        public void TerminateBrowser()
        {
            try
            {
                _driver?.Quit();
                _driver?.Dispose();
            }
            catch
            {
                
            }
        }
        
        private SubscribeResult FollowUnfollow(string path, bool follow = true)
        {
            try
            {
                if (follow)
                {
                    _driver.Navigate().GoToUrl("https://www.instagram.com" + path);

                    Thread.Sleep(2000 + _rand.Next(3000));
                    
                    var subscribeBtn = _driver.FindElementByCssSelector("#react-root button");

                    if (subscribeBtn.Text.Contains("Подписки") || subscribeBtn.Text.Contains("Subscribe"))
                    {
                        return SubscribeResult.Subscribed;
                    }
                    
                    if (subscribeBtn.Text.Contains("одписаться") || subscribeBtn.Text.Contains("ubscribe"))
                    {
                        Actions builder = new Actions(_driver);

                        builder.MoveToElement(subscribeBtn, _rand.Next(0, 5), _rand.Next(0, 5)).Click().Build().Perform();
                    
                        Thread.Sleep(_rand.Next(0, 2000));
                        
                        _restClient.BaseUrl = new Uri("https://www.instagram.com");
                
                        _driver.Navigate().GoToUrl("https://www.instagram.com" + path);

                        _driver.FindElement(By.CssSelector("#react-root button"));
                        
                        if (_driver.PageSource.Contains(">Подписки<") || _driver.PageSource.Contains(">Subscribe<"))
                        {
                            return SubscribeResult.Subscribed;
                        }
                    }
                    else
                    {
                        return SubscribeResult.Error;
                    }
                }
                else
                {
                    _driver.Navigate().GoToUrl("https://www.instagram.com" + path);

                    Thread.Sleep(2000 + _rand.Next(3000));
                    
                    var subscribeBtn = _driver.FindElementByCssSelector("#react-root button");

                    if (subscribeBtn.Text.Contains("Подписаться") || subscribeBtn.Text.Contains("Subscribe"))
                    {
                        return SubscribeResult.Unsubscribed;
                    }
                    
                    if (subscribeBtn.Text.Contains("Подписки") || subscribeBtn.Text.Contains("Unsubscribe"))
                    {
                        subscribeBtn.Click();
                        
                        _driver.Navigate().GoToUrl("https://www.instagram.com" + path);
                        
                        Thread.Sleep(2000);
                        
                        if (_driver.PageSource.Contains(">Подписаться<") && _driver.PageSource.Contains(">Subscribe<"))
                        {
                            return SubscribeResult.Unsubscribed;
                        }
                    }
                    else
                    {
                        return SubscribeResult.Error;
                    }
                }
                
//                
//                var regex = new Regex("id\"\\:\"([0-9]+)\",\"is_private\"");
//                var match = regex.Match(response.Content);
//        
//                var regex2 = new Regex("csrf_token\":\"(.*?)\"");
//                var match2 = regex2.Match(response.Content);
//
//                var referer = "https://www.instagram.com"+ response.Request.Resource;
//                
//                if (match.Success && match2.Success)
//                {
//                    var userId = match.Groups[1].Value;
//
//                    string str = "";
//                    
//                    if (follow)
//                    {
//                        str = "follow";
//                    }
//                    else
//                    {
//                        str = "unfollow";
//                    }
//                    
//                    Thread.Sleep(500 + _rand.Next(0, 1000));
//
//                    response = _restClient.Execute(new RestRequest($"/web/friendships/{userId}/{str}/", Method.POST)
//                        .AddHeader("Accept", "*/*")
//                        .AddHeader("Referer", referer)
//                        .AddHeader("Origin", "https://www.instagram.com")
//                        .AddHeader("X-Instagram-AJAX", "1")
//                        .AddHeader("X-Requested-With", "XMLHttpRequest")
//                        .AddHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"));
// 
//                    Console.WriteLine("Subscribe response: " + response.Content);
//
//                    if (response.Content.Contains("blocked") || response.Content.Contains("блок"))
//                    {
//                        return SubscribeResult.BlockedByInstagram;
//                    }
//                    
//                    var json = JObject.Parse(response.Content);
//
//                    if (follow)
//                    {
//                        switch (json["result"].Value<string>())
//                        {
//                            case "following":
//                                return SubscribeResult.Subscribed;
//                        }
//                    }
//                    else
//                    {
//                        if (json["status"].Value<string>().Equals("ok"))
//                        {
//                            return SubscribeResult.Unsubscribed;
//                        }
//                    }
//                   
//                }
//            
//                return SubscribeResult.Error;
            }
            catch (Exception e)
            {
                return SubscribeResult.Error;
            }

            return SubscribeResult.Error;
        }

        public SubscribeResult Subscribe(string path)
        {
            return FollowUnfollow(path);
        }

        public SubscribeResult Unsubscribe(string path)
        {
            return FollowUnfollow(path, false);
        }

        private int eventCount = 1;
        
        private LikeResult LikeUnlinke(string path, bool like = true)
        {
            try
            {
                //_options.AddArgument("--proxy-server=" + _proxyData.Address.Host + ":" + "44432");
                
                _driver.Navigate().GoToUrl("https://www.instagram.com" + path);

                Thread.Sleep(1000 + _rand.Next(0, 3500));

                if (like)
                {
                    var el = _driver.FindElementByClassName("coreSpriteHeartOpen");
                    
                    Actions builder = new Actions(_driver);

                    builder.MoveToElement(el, _rand.Next(0, 5), _rand.Next(0, 5)).Click().Build().Perform();
                    
                    for (int i = 0; i < 3; i++)
                    {
                        builder = new Actions(_driver);

                        builder.MoveByOffset(_rand.Next(0, 300), _rand.Next(0, 300)).Perform();
                    }
                    
                    Thread.Sleep(1000);
                
                    _driver.Navigate().GoToUrl("https://www.instagram.com" + path);
                    
                    if (_driver.PageSource.Contains("coreSpriteHeartFull"))
                    {
                        return LikeResult.Liked;
                    }
                    else
                    {
                        return LikeResult.Error;
                    }
                }
                else
                {
                    _driver.FindElementByClassName("coreSpriteHeartFull").Click();
                    
                    _driver.Navigate().GoToUrl("https://www.instagram.com" + path);

                    Thread.Sleep(1000);

                    if (_driver.PageSource.Contains("coreSpriteHeartOpen"))
                    {
                        return LikeResult.Unliked;
                    }
                    else
                    {
                        return LikeResult.Error;
                    }
                }
               
//                var regex = new Regex("csrf_token\":\"([A-z0-9]+)\"");
//                var match = regex.Match(response.Content);
//                
//                var regex2 = new Regex("instagram:\\/\\/media\\?id=([0-9]+)");
//                var match2 = regex2.Match(response.Content);
//
//                var referer = "https://www.instagram.com"+ response.Request.Resource;
//            
//                Thread.Sleep(4000 + _rand.Next(0, 2000));
//                
//                if (match2.Success && match.Success)
//                {
//                    var photoId = match2.Groups[1].Value;
//
//                    string str = "";
//                    
//                    if (like)
//                    {
//                        str = "like";
//                    }
//                    else
//                    {
//                        str = "unlike";
//                    }
//
//                    HttpWebRequest request = (HttpWebRequest) WebRequest.Create ($"https://www.instagram.com/web/likes/{photoId}/{str}/");
//                    
//                    request.Method = "POST";
//                    //request.Headers.Add("Accept", "*/*");
//                    request.Accept = "*/*";
//                    request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
//                    request.Headers.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
//                    request.ContentLength = 0;
//                    request.ContentType = "application/x-www-form-urlencoded";
//                    request.Headers.Add("Origin", "https://www.instagram.com");
//                    request.Referer = referer;
//                    request.UserAgent = _restClient.UserAgent;
//                    request.Headers.Add("X-CSRFToken", match.Groups[1].Value);
//                    request.Headers.Add("X-Instagram-AJAX", "1");
//                    request.Headers.Add("X-Requested-With", "XMLHttpRequest");
//                    
//                    request.CookieContainer = _restClient.CookieContainer;
//                    
//                    request.Proxy =  new WebProxy(_proxyData.Address.Host + ":" + _proxyData.Address.Port, false, null, Credentials: new NetworkCredential()
//                    {
//                        UserName = _proxyData.Login,
//                        Password = _proxyData.Password
//                    });
//
//                    string strResponse = "";
//                    Stream dataStream = null;
//                    StreamReader reader = null;
//                    
//                    try
//                    {
//                        dataStream = request.GetResponse().GetResponseStream();
//                        reader = new StreamReader(dataStream);
//                        strResponse = reader.ReadToEnd();
//                    }
//                    finally
//                    {
//                        dataStream?.Close();
//                        reader?.Close();
//                    }
//                   
//                    //onsole.WriteLine(_restClient.CookieContainer.GetCookieHeader(new Uri("https://www.instagram.com")));
////                    var req = new RestRequest($"/web/likes/{photoId}/{str}/", Method.POST)
////                        .AddHeader("Referer", referer).
////                        AddHeader("X-CSRFToken", match.Groups[1].Value)
////                        .AddHeader("X-Instagram-AJAX", "1")
////                        .AddHeader("X-Requested-With", "XMLHttpRequest");
////                    
////                    response = _restClient.Execute(req);
//
//                    if (strResponse.Contains("blocked") || strResponse.Contains("блок"))
//                    {
//                        return LikeResult.BlockedByInstagram;
//                    }
//                    
//                    var json = JObject.Parse(strResponse);
//
//                    if (json["status"].Value<string>().Equals("ok"))
//                    {
//                        if (like)
//                        {
//                            return LikeResult.Liked;
//                        }
//                        else
//                        {
//                            return LikeResult.Unliked;
//                        }
//                    }
//                }
            }
            catch (Exception e)
            {
                return LikeResult.Error;
            }
            
            return LikeResult.Error;
        }
        
        public LikeResult Like(string path)
        {
            return LikeUnlinke(path);
        }

        public LikeResult Unlike(string path)
        {
            return LikeUnlinke(path, false);
        }
    
        private static readonly Semaphore _semaphore = new Semaphore(2, 2);

        public bool ChromeAuth(InstagramCredentials credentials)
        {
            try
            {
                _semaphore.WaitOne();

                ChromeDriverInit();

                if (_proxyData != null)
                {
                    if (!string.IsNullOrEmpty(_proxyData.Login) &&
                             !string.IsNullOrEmpty(_proxyData.Password))
                    {
                         var resp = _driver.ExecuteScript(
                            $"localStorage[\"proxy_login\"] = \"{_proxyData.Login}\";" +
                            $"localStorage[\"proxy_password\"] = \"{_proxyData.Password}\";" +
                            $"localStorage[\"proxy_retry\"] = 5;" +
                            $"localStorage[\"proxy_locked\"] = false;");
                        
                    }
//                    
//                    if (!string.IsNullOrEmpty(_proxyData.Login) &&
//                        !string.IsNullOrEmpty(_proxyData.Password))
//                    {
//                        _driver.Navigate()
//                            .GoToUrl(
//                                "chrome-extension://ggmdpepbjljkkkdaklfihhngmmgmpggp/options.html");
//                        _driver.FindElement(By.Id("login")).SendKeys(_proxyData.Login);
//                        _driver.FindElement(By.Id("password")).SendKeys(_proxyData.Password);
//                        _driver.FindElement(By.Id("save")).Click();
//                    }
                }

                _driver.Navigate().GoToUrl("https://www.instagram.com/accounts/login/");

                _driver.FindElement(By.CssSelector("input[name=username]")).SendKeys(credentials.Login);
                _driver.FindElement(By.CssSelector("input[name=password]")).SendKeys(credentials.Password);

                _driver.FindElement(By.CssSelector("form button")).Submit();

                Thread.Sleep(5100);

                if (!_driver.PageSource.Contains("coreSpriteDesktopNavProfile"))
                {
                    return false;
                }
                else
                {
                    _driver.Navigate().GoToUrl("https://www.instagram.com/");

                    Thread.Sleep(12000);

                    //_restClient.CookieContainer.SetCookies(new Uri("https://www.instagram.com"), "");

                    SyncCookiesWithRestClient();

                    _restClient.AddDefaultHeader("X-CSRFToken",
                        _driver.Manage().Cookies.AllCookies.First(x => x.Name.Equals("csrftoken")).Value);

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            { 
                if (_driver != null)
                {
                    //_driver.Close();
                    //_driver.Dispose();
                } 

                _semaphore.Release();
            }
           
        }
        
        public bool Auth(InstagramCredentials credentials, bool withoutChrome = false)
        {
           try
                {
                    _restClient.CookieContainer.Add(new Cookie() {Name = "ig_or", Value = "landscape-primary", Domain = "www.instagram.com"});
                    _restClient.CookieContainer.Add(new Cookie() {Name = "ig_pr", Value = "1.25", Domain = "www.instagram.com"});
                    _restClient.CookieContainer.Add(new Cookie() {Name = "ig_vh", Value = "772", Domain = "www.instagram.com"});
                    _restClient.CookieContainer.Add(new Cookie() {Name = "ig_vw", Value = "1536", Domain = "www.instagram.com"});

                    var response = _restClient.Execute(new RestRequest("/"));
                
                    var regex = new Regex("csrf_token\":\"([A-z0-9]+)\"");
                    var match = regex.Match(response.Content);
    
                    if (match.Success)
                    {
                        response = _restClient.Execute(new RestRequest("/accounts/login/ajax/", Method.POST)
                            .AddHeader("Referer", "https://www.instagram.com/")
                            .AddHeader("X-Requested-With", "XMLHttpRequest")
                            .AddHeader("X-Instagram-AJAX", "1")
                            .AddHeader("X-CSRFToken", match.Groups[1].Value)
                            .AddParameter("password", credentials.Password)
                            .AddParameter("username", credentials.Login));
    
                        if (response.Content.Contains("/accounts/login/")) return false;
                
                        var json = JObject.Parse(response.Content);
    
                        bool authenticated = json["authenticated"].Value<bool>();
    
                        return authenticated;
                    }
                    else
                    {
                        return false;
                    }
    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    return false;
                }
            return false;
        }

        private ChromeDriver _driver;

        private void SyncCookiesWithChrome()
        {
            var cookies = _restClient.CookieContainer.GetCookies(new Uri("https://www.instagram.com"));

            foreach (Cookie c in cookies)
            {
//                if (c.Domain[0] != '.')
//                {
//                    c.Domain = '.' + c.Domain;
//                }

                _driver.Manage().Cookies.AddCookie(new OpenQA.Selenium.Cookie(c.Name, c.Value, c.Domain, c.Path, c.Expires));
            }
        }

        private void SyncCookiesWithRestClient()
        {
            var cookies = _driver.Manage().Cookies;
            _restClient.CookieContainer = new CookieContainer();
            
            foreach (var c in cookies.AllCookies)
           {
//                string domain = "";
//
//                if (c.Domain[0] != '.')
//                {
//                    domain = '.' + c.Domain;
//                }
//                else
//                {
//                    domain = c.Domain;
//                }
//                
                _restClient.CookieContainer.Add(new Cookie() {
                    Domain = c.Domain.Replace("www.", "."), 
                    Name = c.Name, 
                    Path = c.Path, 
                    Secure = c.Secure, 
                    HttpOnly = c.IsHttpOnly, 
                    Value = c.Value});
            }
        }

        private ChromeOptions _options;
        
        private void ChromeDriverInit()
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            
            service.HideCommandPromptWindow = true;

            _options = new ChromeOptions();
            //options.AddArgument("--window-position=-32000,-32000");
            //options.AddArgument("headless");
            _options.AddArgument("--user-agent=" + _restClient.UserAgent);
            
            if (_proxyData != null)
            {    
                if (!string.IsNullOrEmpty(_proxyData.Login) && !string.IsNullOrEmpty(_proxyData.Password))
                {
                    _options.AddExtension("Proxy-Auto-Auth_v2.0.crx");
                }
                
                _options.AddArgument("--proxy-server=" + _proxyData.Address.Host + ":" + _proxyData.Address.Port);
            }
            
           // _options.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
            
            _driver = new ChromeDriver(service, _options);
            
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        ~InstagramBot()
        {
            //TerminateBrowser();
        }
        
        private void AdditionalRequest()
        {
             var resp = _restClient.Execute(new RestRequest("/qp/fetch_web/", Method.POST).
                    AddParameter("query", "viewer()+{ ++eligible_promotions.surface_nux_id(<surface>).external_gating_permitted_qps(<external_gating_permitted_qps>)+{ ++++edges+{ ++++++priority, ++++++time_range+{ ++++++++start, ++++++++end ++++++}, ++++++node+{ ++++++++id, ++++++++promotion_id, ++++++++max_impressions, ++++++++triggers, ++++++++template+{ ++++++++++name, ++++++++++parameters+{ ++++++++++++name, ++++++++++++string_value ++++++++++} ++++++++}, ++++++++creatives+{ ++++++++++title+{ ++++++++++++text ++++++++++}, ++++++++++content+{ +++…+{ ++++++++++++++text ++++++++++++}, ++++++++++++url, ++++++++++++limit, ++++++++++++dismiss_promotion ++++++++++}, ++++++++++secondary_action{ ++++++++++++title+{ ++++++++++++++text ++++++++++++}, ++++++++++++url, ++++++++++++limit, ++++++++++++dismiss_promotion ++++++++++}, ++++++++++dismiss_action{ ++++++++++++title+{ ++++++++++++++text ++++++++++++}, ++++++++++++url, ++++++++++++limit, ++++++++++++dismiss_promotion ++++++++++}, ++++++++++image+{ ++++++++++++uri ++++++++++} ++++++++} ++++++} ++++} ++} }").
                    AddParameter("surface_param", "5095").
                    AddParameter("vc_policy", "default").
                    AddParameter("version", 1).
                    AddHeader("X-Instagram-AJAX", "1")
                    .AddHeader("X-Requested-With", "XMLHttpRequest"));
                                        
            Thread.Sleep(1000);
            
            CookieCollection collection = _restClient.CookieContainer.GetCookies(new Uri("https://www.instagram.com"));

            string mid = "";
            
            foreach (Cookie v in collection)
            {
                if (v.Name.Equals("mid"))
                {
                    mid = v.Value;
                    break;
                }
            }
            
            Thread.Sleep(1000);

            long ts = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            
            resp = _restClient.Execute(new RestRequest("/ajax/bz", Method.POST).
                AddParameter("q", "[{\"page_id\":\"a0v2f7\",\"posts\":[[\"qe:expose\",{\"qe\":\"save\",\"mid\":\""+ mid + "\"},"+ts+",0]],\"trigger\":\"pigeon\",\"send_method\":\"ajax\"}]").
                AddParameter("ts", ts));
            
            Console.WriteLine(resp.Content);
            
            Thread.Sleep(1000);
            
            ts = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            
            resp = _restClient.Execute(new RestRequest("/ajax/bz", Method.POST).
                AddParameter("q", "[{\"page_id\":\"a0v2f7\",\"posts\":[[\"pigeon_failed\",{\"event_count\":"+eventCount+"},"+ts+",0]]}]").
                AddParameter("ts", ts));
            
            Console.WriteLine(resp.Content);

            var rsp = _restClient.Execute(new RestRequest("/"));
            
            var rx = new Regex("(/([a-z0-9_-]{10,30}).(js|jpg|jpeg))");
            var mx = rx.Matches(rsp.Content);

            var list = new List<Match>();
            
            foreach (Match t in mx)
            {
                list.Add(t);
            }
            
            Parallel.ForEach(list, new ParallelOptions() {MaxDegreeOfParallelism = 5}, m =>
            {
                _restClient.Execute(new RestRequest(m.Groups[1].Value));
                Console.WriteLine(m.Groups[1].Value);
            });
        }

        public string UserAgent
        {
            set { _restClient.UserAgent = value; }
        }

        private ProxyData _proxyData;
        
        public ProxyData ProxyData
        {
            set
            {
                if (value != null)
                {
                    _restClient.Proxy = new WebProxy(value.Address.Host + ":" + value.Address.Port, false, null, Credentials: new NetworkCredential()
                    {
                        UserName = value.Login,
                        Password = value.Password
                    });

                    _proxyData = value;
                }
            }
        }

        public InstagramBot()
        {
            _restClient = new RestClient("https://www.instagram.com");

            _restClient.CookieContainer = new CookieContainer();
        }
    }
}