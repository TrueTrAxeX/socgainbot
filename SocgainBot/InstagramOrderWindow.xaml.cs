﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SocgainBot.Core.Bot.Instagram;
using SocgainBot.Core.Database.Model;

namespace SocgainBot
{
    public partial class InstagramOrderWindow
    {
        private InstagramSocgainBot _sbot;
        private InstagramBot _bot;
        private UserAccount _account;

        private InstagramSocgainBot.OrderSubscribersResult
            result;
        
        public InstagramOrderWindow(UserAccount account, InstagramBot bot, InstagramSocgainBot sbot, int balance)
        {
            InitializeComponent();

            Score.Text = balance.ToString();

            _sbot = sbot;
            _bot = bot;
            _account = account;

            SubmitBtn.Click += async (sender, args) =>
            {
                try
                {
                    var item = (ComboBoxItem) Price.SelectedItem;
                    int price = int.Parse(item.Content.ToString());
                    int count = int.Parse(Count.Text);
                    var text = InstagramNickname.Text;

                    SubmitBtn.IsEnabled = false;
                    SubmitBtn.Content = "Пожалуйста подождите...";

                    bool task = await Task.Factory.StartNew<bool>(() =>
                    {
                        if (balance < count*price)
                        {
                            balance = (_sbot.GetBalance() ?? 0);
                        
                            MessageBox.Show("Не хватает баланса, чтобы сделать заказ!", "Ошибка", MessageBoxButton.OK,
                                MessageBoxImage.Error);

                            return false;
                        }

                        return true;
                    });

                    if (!task)
                    {
                        return;
                    }
                    
                    result = InstagramSocgainBot.OrderSubscribersResult.Error;

                    var t = Task.Factory.StartNew(() =>
                    {
                        result = _sbot.OrderSubscribers(price, count, text);
                    });

                    await t;
                    
                    SubmitBtn.Content = "Заказать";
                    SubmitBtn.IsEnabled = true;

                    if (result == InstagramSocgainBot.OrderSubscribersResult.Success)
                    {
                        MessageBox.Show("Накрутка успешно заказана!", "Успех", MessageBoxButton.OK,
                            MessageBoxImage.Information);
                        
                        balance = (_sbot.GetBalance() ?? 0);
                    }
                    else if (result == InstagramSocgainBot.OrderSubscribersResult.NotEnoughBalance)
                    {
                        MessageBox.Show("Не хватает баланса, чтобы сделать заказ!", "Ошибка", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                    else if (result == InstagramSocgainBot.OrderSubscribersResult.TooSmallAmount)
                    {
                        MessageBox.Show("Минимальное количество заказа должно быть не менее 10 подписок", "Ошибка",
                            MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                    else if (result == InstagramSocgainBot.OrderSubscribersResult.Error)
                    {
                        MessageBox.Show("Неизвестная ошибка", "Ошибка", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }
                    else if (result == InstagramSocgainBot.OrderSubscribersResult.UserNotFound)
                    {
                        MessageBox.Show("Пользователь на которого вы собираетесь сделать заказ не найден", "Ошибка",
                            MessageBoxButton.OK,
                            MessageBoxImage.Error);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MessageBox.Show("Проверьте правильность введенных данных!", "Ошибка", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
                finally
                {
                    SubmitBtn.Content = "Заказать";
                    SubmitBtn.IsEnabled = true;
                    Score.Text = balance.ToString();
                }
            };
        }
    }
}
