﻿using System;
using Program.Core.Utils;

namespace SocgainBot.Core.Settings
{
    public class InstagramSettings
    {
        public static int MaxLikesInDay
        {
            get {
                try
                {
                    return (int) ConfigService.Instance["inst_max_likes_in_day"];
                }
                catch
                {
                    return 200;
                }
            }

            set { ConfigService.Instance["inst_max_likes_in_day"] = value; }
        }

        public static int MaxSubscribesInDay
        {
            get
            {
                try
                {
                    return (int) ConfigService.Instance["inst_max_subscribes_in_day"];
                }
                catch
                {
                    return 50;
                }
            }
            
            set { ConfigService.Instance["inst_max_subscribes_in_day"] = value; }
        }
        
        public static bool EnableLike
        {
            get {
                try
                {
                    return (bool) ConfigService.Instance["inst_enable_like"];
                }
                catch
                {
                    return false;
                }
            }

            set { ConfigService.Instance["inst_enable_like"] = value; }
        }

        public static bool EnableSubscribe
        {
            get {
                try
                {
                    return (bool) ConfigService.Instance["inst_enable_subscribe"];
                }
                catch
                {
                    return false;
                }
            }

            set { ConfigService.Instance["inst_enable_subscribe"] = value; }
        }

        public static int BotThreadCount 
        {
            get {
                try
                {
                    return (int) ConfigService.Instance["inst_bot_thread_count"];
                }
                catch
                {
                    return 1;
                }
            }

            set { ConfigService.Instance["inst_bot_thread_count"] = value; }
        }

        public static string DefaultUserAgent
        {
            get {
                try
                {
                    return (string) ConfigService.Instance["inst_defualt_user_agent"];
                }
                catch
                {
                    return null;
                }
            }

            set { ConfigService.Instance["inst_defualt_user_agent"] = value; }
        }
    }
}