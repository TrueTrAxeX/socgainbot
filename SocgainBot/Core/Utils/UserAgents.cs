﻿using System.Collections.Generic;

namespace SocgainBot.Core.Utils
{
    public class UserAgents
    {
        public static Dictionary<string, string> List = new Dictionary<string, string>()
        {
            {"Chrome (Win 10)", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36"},
            {"Opera (Win 10)", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 OPR/51.0.2830.34"},
            {"Firefox (Win 10)", "Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0"},
            {"Opera 12.00 (id as 9.8) (Win 7)", "Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00"},
            {"Chrome 20.0.1090.0 (Win 8)", "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6"},
            {"Safari 536.26.17 (6) (OS X 10_7_5 Intel)", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML like Gecko) Version/6.0.2 Safari/536.26.17"},
            {"Firefox 12.0 Linux (32 bit)", "Mozilla/5.0 (X11; Linux i686; rv:12.0) Gecko/20100101 Firefox/12.0"}
        };
    }
}