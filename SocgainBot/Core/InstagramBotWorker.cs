﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using AngleSharp.Services;
using SocgainBot.Core.Bot.Instagram;
using SocgainBot.Core.Database;
using SocgainBot.Core.Database.Model;
using SocgainBot.Core.Settings;
using SocgainBot.Core.Utils;
using Brush = System.Drawing.Brush;
using Brushes = System.Drawing.Brushes;

namespace SocgainBot.Core
{
    public class InstagramBotWorker
    {
        public static bool IsWorking { get; set; } = false;

        private static Thread _thread;
        
        private static MainWindow _mainWindow;

        private static readonly object Locker = new object();

        public class BotContainer
        {
            public InstagramBot InstagramBot { get; set; }
            public InstagramSocgainBot InstagramSocgainBot { get; set; }
        }
        
        public static ConcurrentDictionary<Guid, BotContainer> _botDictionary = new ConcurrentDictionary<Guid, BotContainer>();
        
        private static Dictionary<Guid, DateTime> _subscribeCooldowns = new Dictionary<Guid, DateTime>();
        
        private static ConcurrentQueue<Thread> _currentForeachThreads = new ConcurrentQueue<Thread>();
        
        private static ConcurrentDictionary<Guid, DateTime> _likeCooldown = new ConcurrentDictionary<Guid, DateTime>();
        private static ConcurrentDictionary<Guid, DateTime> _subscribeCooldown = new ConcurrentDictionary<Guid, DateTime>();

        private static readonly Random _random = new Random();
        
        private static readonly HashSet<string> _xCHashSet = new HashSet<string>();

        private static bool TaskMethod(UserAccount acc, CancellationToken token)
        {
            try
            {
                if (acc.Enabled)
                {
                    string userAgent = null;

                    if (InstagramSettings.DefaultUserAgent == null)
                    {
                        userAgent = UserAgents.List.FirstOrDefault().Value;
                    }
                    else
                    {
                        userAgent = UserAgents.List[InstagramSettings.DefaultUserAgent];
                    }

                    if (!string.IsNullOrEmpty(acc.BrowserUserAgent))
                    {
                        userAgent = acc.BrowserUserAgent;
                    }

                    if (!_botDictionary.ContainsKey(acc.Id))
                    {
                        _botDictionary[acc.Id] = new BotContainer()
                        {
                            InstagramBot = new InstagramBot()
                        };

                        _botDictionary[acc.Id].InstagramSocgainBot =
                            new InstagramSocgainBot(_botDictionary[acc.Id].InstagramBot);

                        _botDictionary[acc.Id].InstagramBot.ProxyData = acc.ProxyData;
                        _botDictionary[acc.Id].InstagramSocgainBot.ProxyData = acc.ProxyData;
                    }

                    var bot = _botDictionary[acc.Id].InstagramBot;
                    var sbot = _botDictionary[acc.Id].InstagramSocgainBot;

                    if (userAgent != null)
                    {
                        bot.UserAgent = userAgent;
                        sbot.UserAgent = userAgent;
                    }

                    if (!bot.IsAuthenticated)
                    {
                        if (token.IsCancellationRequested) return false;
                        
                        if (bot.ChromeAuth(new InstagramBot.InstagramCredentials()
                        {
                            Login = acc.Login,
                            Password = acc.Password
                        }))
                        {
                            Log($"<font color='green'>Успешно авторизовались в Instagram (аккаунт: <b>{acc.Login}</b>)</font>");
                            bot.IsAuthenticated = true;
                        }
                        else
                        {
                            Log(
                                $"<font color='red'>Не удалось авторизоваться в Instagram (аккаунт: <b>{acc.Login}</b>)</font>");
                            return false;
                        }
                        
                        if (token.IsCancellationRequested) return false;
                    }

                    if (!sbot.IsAuthenticated)
                    {
                        if (token.IsCancellationRequested) return false;
                        
                        if (sbot.Auth())
                        {
                            sbot.IsAuthenticated = true;
                            Log($"<font color='green'>Успешно авторизовались в Socgain.com (аккаунт: <b>{acc.Login}</b>)</font>");
                        }
                        else
                        {
                            Log(
                                $"<font color='red'>Не удалось авторизоваться в Socgain.com (аккаунт: <b>{acc.Login}</b>)</font>");
                            return false;
                        }
                        
                        if (token.IsCancellationRequested) return false;
                    }

                    if (sbot.IsAuthenticated)
                    {
                        if (acc.LastLikeTime + TimeSpan.FromHours(1) < DateTime.Now)
                        {
                            acc.TotalLikesInDay = 0;
                        }

                        if (acc.LastSubscribeTime + TimeSpan.FromHours(1) < DateTime.Now)
                        {
                            acc.TotalSubscribesInDay = 0;
                        }

                        if (!_xCHashSet.Contains($"l:::{acc.Login}"))
                        {
                            if (InstagramSettings.EnableLike && acc.TotalLikesInDay >= InstagramSettings.MaxLikesInDay)
                            {
                                _xCHashSet.Add($"l:::{acc.Login}");
                                
                                Log($"Достигнут лимит лайков для аккаунта <b>{acc.Login}</b>");
                            }
                        }

                        if (!_xCHashSet.Contains($"s:::{acc.Login}"))
                        {
                            if (InstagramSettings.EnableSubscribe && acc.TotalSubscribesInDay >= InstagramSettings.MaxSubscribesInDay)
                            {
                                _xCHashSet.Add($"s:::{acc.Login}");
                            
                                Log($"Достигнут лимит подписок для аккаунта <b>{acc.Login}</b>");
                            }
                        }
                        
                        if (InstagramSettings.EnableLike &&
                            acc.TotalLikesInDay < InstagramSettings.MaxLikesInDay)
                        {
                            bool flag = false;
                            
                            if (_likeCooldown.ContainsKey(acc.Id))
                            {
                                if (_likeCooldown[acc.Id] +
                                    TimeSpan.FromMilliseconds(acc.DelayAfterLikeTask + _random.Next(0, 3000)) > DateTime.Now)
                                {
                                    flag = true;
                                }
                            }

                            if (!flag)
                            {
                                if (token.IsCancellationRequested) return false;
                                
                                var r = sbot.DoLikeTask();

                                if (token.IsCancellationRequested) return false;
                                
                                _likeCooldown[acc.Id] = DateTime.Now;

                                if (r.Status == InstagramSocgainBot.LikeTaskStatus.Success)
                                {
                                    var nowTime = DateTime.Now;

                                    if (acc.LastLikeTime.Hour != nowTime.Hour ||
                                        acc.LastLikeTime.DayOfYear != nowTime.DayOfYear)
                                    {
                                        acc.TotalLikesInDay = 0;
                                        acc.LastLikeTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        acc.TotalLikesInDay++;
                                    }
                                    
                                    if (_xCHashSet.Contains($"l:::{acc.Login}"))
                                    {
                                        _xCHashSet.Remove($"l:::{acc.Login}");
                                    }
                                    
                                    Log(
                                        $"Бот успешно лайкнул фото {r.Url} за <b>{r.Code}</b> баллов (аккаунт: <b>{acc.Login}</b>)");
                                }
                                else if (r.Status == InstagramSocgainBot.LikeTaskStatus.BlockedByInstagram)
                                {
                                    Log(
                                        $"<font color='red'>Инстаграм заблокировал лайк от пользователя <b>{acc.Login}</b></font>");
                                }
                                else if (r.Status == InstagramSocgainBot.LikeTaskStatus
                                             .TasksNotAvailable)
                                {
                                    Log($"Для аккаунта <b>{acc.Login}</b> нет доступных заданий для соверешния лайков. Ждем...");
                                }
                                else
                                {
                                    Log(
                                        $"Боту не удалось лайкнуть фото {r.Url} (аккаунт: <b>{acc.Login}</b>) ({r.Code})");
                                }
                            }
                            
                        }
    
                        if (InstagramSettings.EnableSubscribe && acc.TotalSubscribesInDay <
                            InstagramSettings.MaxSubscribesInDay)
                        {
                            bool flag = false;

                            if (_subscribeCooldowns.ContainsKey(acc.Id))
                            {
                                if ((_subscribeCooldowns[acc.Id] + TimeSpan.FromMinutes(5)) >
                                    DateTime.Now)
                                {
                                    flag = true;
                                }
                            }
                            
                            bool flag2 = false;
                            
                            if (_subscribeCooldown.ContainsKey(acc.Id))
                            {
                                if (_subscribeCooldown[acc.Id] +
                                    TimeSpan.FromMilliseconds(acc.DelayAfterSubscribeTask) > DateTime.Now)
                                {
                                    flag2 = true;
                                }
                            }

                            if (!flag && !flag2)
                            {
                                if (token.IsCancellationRequested) return false;
                                
                                var r = sbot.DoSubscribeTask();
                                
                                if (token.IsCancellationRequested) return false;
                                
                                _subscribeCooldown[acc.Id] = DateTime.Now;
                                
                                if (r.Status == InstagramSocgainBot.SubscribeTaskStatus.Success)
                                {
                                    var nowTime = DateTime.Now;

                                    if (acc.LastSubscribeTime.Hour != nowTime.Hour ||
                                        acc.LastSubscribeTime.DayOfYear != nowTime.DayOfYear)
                                    {
                                        acc.LastSubscribeTime = DateTime.Now;
                                        acc.TotalSubscribesInDay = 0;
                                    }
                                    else
                                    {
                                        acc.TotalSubscribesInDay++;
                                    }

                                    if (_xCHashSet.Contains($"s:::{acc.Login}"))
                                    {
                                        _xCHashSet.Remove($"s:::{acc.Login}");
                                    }
                                    
                                    Log(
                                        $"Бот успешно подписался на пользователя {r.Url} за <b>{r.ScoreGain}</b> баллов (аккаунт: <b>{acc.Login}</b>)");
                                }
                               
                                else if (r.Status == InstagramSocgainBot.SubscribeTaskStatus
                                             .BotDetected)
                                {
                                    Log(
                                        $"Нас посчитали за бота, пожалуйста накрутите более 3 подписчиков на аккаунт <b>{acc.Login}</b>");
                                    return false;
                                }
                                else if (r.Status == InstagramSocgainBot.SubscribeTaskStatus
                                             .BlockedByInstagram)
                                {
                                    Log(
                                        $"<font color='red'>Инстаграм заблокировал подписку от пользователя <b>{acc.Login}</b></font>");
                                }
                                else if (r.Status == InstagramSocgainBot.SubscribeTaskStatus
                                             .SubscribeLimit)
                                {
                                    _subscribeCooldowns[acc.Id] = DateTime.Now;
                                    Log(
                                        $"Нет доступных заданий на подписку, пауза 5 минут (аккаунт: <b>{acc.Login}</b>)");
                                }
                                else
                                {
                                    Log(
                                        $"Боту не удалось подписаться на пользователя {r.Url} (аккаунт: <b>{acc.Login}</b>)");
                                }
                            }
                        }

                        DatabaseManager.UserAccountCollection.Update(acc);
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception " + e.StackTrace);
                
                Log(
                    $"В боте произошла критическая ошибка!");
            }

            return true;
        }
        
        private static CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        
        private static readonly List<Task> ActiveTasks = new List<Task>();

        public static void Start(MainWindow mainWindow)
        {
            lock (Locker)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                
                 _mainWindow = mainWindow;
            
                if (IsWorking) return;
                
                IsWorking = true;
                
                _thread = new Thread(() =>
                {
                    List<UserAccount> accounts =
                        DatabaseManager.UserAccountCollection.Find(x => x.AccountType == AccountType.Instagram).ToList();

                    Semaphore semaphore = new Semaphore(InstagramSettings.BotThreadCount, InstagramSettings.BotThreadCount);
            
                    CancellationToken token = _cancellationTokenSource.Token;

                    token.Register(() =>
                    {
                        foreach (var e in _botDictionary)
                        {
                            e.Value.InstagramBot.TerminateBrowser();
                        }
                    });
                    
                    try
                    {
                        Queue<UserAccount> accs = new Queue<UserAccount>(accounts);

                        while (true)
                        {
                            if (token.IsCancellationRequested)
                            {
                                return;
                            }
                            
                            semaphore.WaitOne();

                            Task task = null;

                            task = Task.Factory.StartNew(() =>
                            {
                                try
                                {
                                    if (accs.Count == 0)
                                    {
                                        Thread.Sleep(1000);
                                        return;
                                    }
                                    
                                    var acc = accs.Dequeue();
                                    
                                    Console.WriteLine("Dequeue: " + acc.Login);
                                    
                                    bool result = TaskMethod(acc, token);

                                    if (result)
                                    {
                                        var _acc = DatabaseManager.UserAccountCollection.FindOne(x => x.Id.Equals(acc.Id));
                                        
                                        if(_acc != null && _acc.Enabled)
                                            accs.Enqueue(_acc);
                                    }
                                    else
                                    {
                                        accounts.Remove(acc);
                                    }
                                }
                                finally
                                {
                                    ActiveTasks.Remove(task);
                                    semaphore.Release();
                                }
                            });
                            
                            ActiveTasks.Add(task);
                            
                            Thread.Sleep(1000);
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Thread interrupted!");
                    }
                });
        
                _thread.Start();
            }
        }

        private static void Log(string message, string backgroundColor = "#FFFFFF")
        {
            if (_mainWindow != null)
            {
                _mainWindow.InstLogBox.Dispatcher.Invoke(DispatcherPriority.Background,
                    new Action(() =>
                    {
                        _mainWindow.InstLogBox.Items.Insert(0,
                            new ListBoxItem() {Content = new TheArtOfDev.HtmlRenderer.WPF.HtmlLabel() {Text = "<span style='background-color:"+backgroundColor+";font-size:12px;'>" + (DateTime.Now.ToString("G") + ": " + message) + "</span>"}});

                        if (_mainWindow.InstLogBox.Items.Count > 300)
                        {
                            _mainWindow.InstLogBox.Items.RemoveAt(300);
                        }
                    }));
            }
        }
        
        [SecurityPermission(SecurityAction.Demand, ControlThread = true)]
        public static void Stop()
        {
            foreach (var e in _botDictionary)
            {
                e.Value.InstagramBot.TerminateBrowser();
            }
            
            _botDictionary.Clear();

            lock (Locker)
            {
                IsWorking = false;
                
                _cancellationTokenSource.Cancel();

                while (ActiveTasks.Any(x => !x.IsCompleted))
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Wait tasks complete");
                }
            }
        }
    }
}