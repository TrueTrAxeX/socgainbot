﻿using System;
using System.ComponentModel.Design;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using AngleSharp;
using AngleSharp.Extensions;
using AngleSharp.Parser.Html;
using RestSharp;
using SocgainBot.Core.Database.Model;

namespace SocgainBot.Core.Bot.Instagram
{
    public class InstagramSocgainBot
    {
        public bool IsAuthenticated { get; set; }

        public string UserAgent
        {
            set { _restClient.UserAgent = value; }
        } 

        private InstagramBot _instagramBot;
        
        private HtmlParser _htmlParser = new HtmlParser();

        private RestClient _restClient;
        
        public ProxyData ProxyData
        {
            set
            {
                if (value != null)
                {
                    _restClient.Proxy = new WebProxy(value.Address.Host + ":" + value.Address.Port, false, null, Credentials: new NetworkCredential()
                    {
                        UserName = value.Login,
                        Password = value.Password
                    });
                }
                else
                {
                    _restClient.Proxy = null;
                }

               
            }
        }

        public InstagramSocgainBot(InstagramBot bot)
        {
            _instagramBot = bot;
            
            _restClient = new RestClient("https://www.instagram.com");
            
            _restClient.CookieContainer = new CookieContainer();
            _restClient.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 OPR/51.0.2830.34";
          
            //_restClient.Timeout = 60000;
            //_restClient.ReadWriteTimeout = 60000;
        }

        public enum LikeTaskStatus
        {
            Success, Error, TasksNotAvailable, BlockedByInstagram
        }
        
        public class LikeTaskResult
        {
            public LikeTaskStatus Status { get; set; }
            public string Url { get; set; }
            public int Code { get; set; }
        }

        public enum SubscribeTaskStatus
        {
            Success, Error, SubscribeLimit, BotDetected, BlockedByInstagram
        }
        
        public class SubscribeTaskResult
        {
            public SubscribeTaskStatus Status { get; set; }
            public string Url { get; set; }
            public int ScoreGain { get; set; }
        }

        public SubscribeTaskResult DoSubscribeTask()
        {
            string _url = null;
            int? _cpc = null;
            
            try
            {
                _restClient.BaseUrl = new Uri("http://socgain.com");
                var response = _restClient.Execute(new RestRequest($"/follow"));

                if (response.Content.Contains("Вы дальше можете подписываться через"))
                {
                    return new SubscribeTaskResult() {Status = SubscribeTaskStatus.SubscribeLimit};
                }

                if (response.Content.Contains("Сервис считает, что вы бот"))
                {
                    return new SubscribeTaskResult() {Status = SubscribeTaskStatus.BotDetected};
                }
                
                var angle = _htmlParser.Parse(response.Content);

                var elements = angle.QuerySelectorAll(".user-subscribe-block");
                
                var attr = elements.ElementAt(new Random().Next(0, elements.Length-1)).QuerySelector("a.blue-button").GetAttribute("onclick");
                    
                var regex = new Regex("openFll\\(([0-9]+),'(.*?)',([0-9]+)\\);");
                var matcher = regex.Match(attr);

                if (matcher.Success)
                {
                    var name = matcher.Groups[2].Value;
                    int cpc = int.Parse(matcher.Groups[3].Value);
                    int id = int.Parse(matcher.Groups[1].Value);
                        
                    _restClient.BaseUrl = new Uri("http://dnal.land");

                    response = _restClient.Execute(new RestRequest($"/follow_user.php?name={name}"));
                        
                    var regex2 = new Regex("location\\.replace\\(\"(.*?)\"\\)");
                    var match2 = regex2.Match(response.Content);

                    if (match2.Success)
                    {
                        var url = match2.Groups[1].Value.Replace("https://instagram.com", "");

                        var result = _instagramBot.Subscribe(url);

                        if (result == InstagramBot.SubscribeResult.BlockedByInstagram)
                        {
                            
                            _restClient.BaseUrl = new Uri("http://socgain.com");

                            response = _restClient.Execute(new RestRequest($"/check/check_fll.php?id={id}"));

                            
                            return new SubscribeTaskResult() {ScoreGain = cpc, Status = SubscribeTaskStatus.BlockedByInstagram, Url = url};
                        }
                       
                        _restClient.BaseUrl = new Uri("http://socgain.com");

                        response = _restClient.Execute(new RestRequest($"/check/check_fll.php?id={id}"));
                        
                        int code = int.Parse(response.Content);

                        _url = url;
                        _cpc = cpc;
                        
                        Console.WriteLine("Code");
                        
                        if (code == 1 || code == 3)
                        {
                            response = _restClient.Execute(new RestRequest($"/edit/deletefoll?foll="+id));

                            if (code == 1)
                            {
                                return new SubscribeTaskResult() {ScoreGain = cpc, Status = SubscribeTaskStatus.Success, Url = url};
                            }
                            else
                            {
                                _instagramBot.Unsubscribe(url);
                            }
                        }
                        else
                        {
                            return new SubscribeTaskResult() {ScoreGain = cpc, Status = SubscribeTaskStatus.Error, Url = url};
                        }

                    }
                }
            }
            catch (Exception e)
            {
                return new SubscribeTaskResult() {Status = SubscribeTaskStatus.Error, ScoreGain = (_cpc ?? -1), Url = _url};
            }
            
            return new SubscribeTaskResult() {Status = SubscribeTaskStatus.Error, ScoreGain = (_cpc ?? -1), Url = _url};
        }

        private Random _random = new Random();
        
        public LikeTaskResult DoLikeTask()
        {
            try
            {
                _restClient.BaseUrl = new Uri("http://socgain.com");
                var response = _restClient.Execute(new RestRequest($"elike"));

                var angle = _htmlParser.Parse(response.Content);

                var sel = angle.QuerySelector(".content a.blue-button.pull-left");

                var attr = sel.GetAttribute("onclick");

                var regex = new Regex("openLikeWin\\(([0-9]+),");
                var matcher = regex.Match(attr);

                if (matcher.Success)
                {
                    var num = matcher.Groups[1].Value;

                    var res = _restClient.Execute(new RestRequest("/waits.php"));

                    Thread.Sleep(1000);
                    
                    var regex2 = new Regex("<a href='(.*?)'");
                    var match2 = regex2.Match(res.Content);

                    if (match2.Success)
                    {
                        _restClient.BaseUrl = new Uri("http://dailygram.ru");   
                        _restClient.Execute(new RestRequest("/inst_wait.php?link=" + match2.Groups[1].Value));
                        
                        var instagramUrlPath = match2.Groups[1].Value.Replace("http://instagram.com", "");
                    
                        Console.WriteLine("Instagram url path: " + instagramUrlPath);

                        if (string.IsNullOrEmpty(instagramUrlPath) || instagramUrlPath.Length < 3)
                        {
                            return new LikeTaskResult() {Code = 0, Status = LikeTaskStatus.TasksNotAvailable};
                        }
                        
                        var result = _instagramBot.Like(instagramUrlPath);

                        if (result == InstagramBot.LikeResult.BlockedByInstagram)
                        {
                            _restClient.BaseUrl = new Uri("http://socgain.com");

                            response = _restClient.Execute(new RestRequest($"/check/check_likes.php").AddHeader("X-Requested-With", "XMLHttpRequest").AddHeader("Referer", "http://socgain.com/elike"));

                            return new LikeTaskResult() {Status = LikeTaskStatus.BlockedByInstagram, Code = -1, Url = "http://instagram.com" + instagramUrlPath};
                        }
                        
                        Console.WriteLine("Like status: " + result);

                        _restClient.CookieContainer = _instagramBot.RestClient.CookieContainer;
                        
                        Thread.Sleep(500 + _random.Next(1000));
                        
                        _restClient.BaseUrl = new Uri("http://socgain.com");

                        response = _restClient.Execute(new RestRequest($"/check/check_likes.php").AddHeader("X-Requested-With", "XMLHttpRequest").AddHeader("Referer", "http://socgain.com/elike"));
                    
                        var r = int.Parse(response.Content);
                         
                        if (r == 3 || r == 2 || r == 1)
                        {
                            return new LikeTaskResult() {Status = LikeTaskStatus.Success, Code = r, Url = "http://instagram.com" + instagramUrlPath};
                        }
                        else
                        {
                            if (result == InstagramBot.LikeResult.Liked)
                            {
                                _instagramBot.Unlike(instagramUrlPath);
                            }
                            
                            return new LikeTaskResult() {Status = LikeTaskStatus.Error, Code = r, Url = "http://instagram.com" + instagramUrlPath};
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new LikeTaskResult() {Status = LikeTaskStatus.Error};
            }

            return new LikeTaskResult() {Status = LikeTaskStatus.Success};
        }

        public enum OrderSubscribersResult
        {
            TooSmallAmount, Error, NotEnoughBalance, Success, UserNotFound
        }

        public OrderSubscribersResult OrderSubscribers(int price, int count, string userName)
        {
            try
            {
                _restClient.BaseUrl = new Uri("http://socgain.com");

                _restClient.FollowRedirects = false;

                var response = _restClient.Execute(new RestRequest("/spend/spender.php", Method.POST)
                    .AddParameter("bal", price.ToString())
                    .AddParameter("kol", count.ToString())
                    .AddParameter("lang", "--Все--")
                    .AddParameter("tags", "#like,#follow")
                    .AddParameter("user", userName));

                if (response.StatusCode == HttpStatusCode.Found)
                {
                    string location = response.Headers.First(x => x.Name.Equals("Location")).Value.ToString();

                    if (location.Contains("al=5"))
                    {
                        return OrderSubscribersResult.TooSmallAmount;
                    }

                    if (location.Contains("al=3"))
                    {
                        return OrderSubscribersResult.UserNotFound;
                    }

                    if (location.Contains("al=6"))
                    {
                        return OrderSubscribersResult.NotEnoughBalance;
                    }

                    if (location.Contains("al=7"))
                    {
                        var res = _restClient.Execute(new RestRequest("/earn#tab2"));
                        var doc = _htmlParser.Parse(res.Content);

                        var tbody = doc.QuerySelector("#data-table-zakaz tbody");

                        var regex = new Regex("/edit/deleter\\?id=([0-9]+)");
                        
                        foreach (var tr in tbody.QuerySelectorAll("tr"))
                        {
                            var match = regex.Match(tr.InnerHtml);

                            var sel = tr.QuerySelector(".row .col-md-12 a");

                            if (sel != null)
                            {
                                if (sel.TextContent.Contains(userName))
                                {
                                    if (match.Success)
                                    {
                                        var link = "/edit/deleter?id=" + match.Groups[1].Value;
                                        
                                        _restClient.Execute(new RestRequest(link));

                                        return OrderSubscribers(price, count, userName);
                                        
                                    }
                                }
                            }
                        }
                    }

                    if (location.Contains("al=16"))
                    {
                        return OrderSubscribersResult.Success;
                    }
                }
                else
                {
                    return OrderSubscribersResult.Error;
                }
            }
            catch (Exception e)
            {
                return OrderSubscribersResult.Error;
            }
            finally
            {
                _restClient.FollowRedirects = true;
            }
            
            return OrderSubscribersResult.Error;
        }
        
        public int? GetBalance()
        {
            try
            {
                _restClient.BaseUrl = new Uri("http://socgain.com");
            
                var response = _restClient.Execute(new RestRequest("/"));

                var doc = _htmlParser.Parse(response.Content);
                var sel = doc.QuerySelector("span.bal-value div");

                if (sel != null)
                {
                    return int.Parse(sel.TextContent);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool Auth()
        {
            try
            {
                _restClient.CookieContainer = _instagramBot.RestClient.CookieContainer;
                _restClient.BaseUrl = new Uri("https://ulogin.ru");
                _restClient.FollowRedirects = false;
                
                var response = _restClient.Execute(new RestRequest("/auth.php?name=instagram&window=0&lang=ru&fields=&force_fields=&optional=&redirect_uri=http://socgain.com/ulogin-proverka.php&host=socgain.com&page=http://socgain.com/&callback=&verify=&mobile=1&m=1&screen=414x736&altway=1&q="));
                
                string redirectAddr = (string) response.Headers.First(x => x.Name.Equals("Location")).Value;
                redirectAddr = redirectAddr.Replace("https://api.instagram.com/", "");
    
                _restClient.BaseUrl = new Uri("https://api.instagram.com/");
    
                response = _restClient.Execute(new RestRequest(redirectAddr));
                
                redirectAddr = (string) response.Headers.First(x => x.Name.Equals("Location")).Value;
                redirectAddr = redirectAddr.Replace("https://instagram.com/", "").Replace("https://www.instagram.com/", "");
                
                _restClient.BaseUrl = new Uri("https://www.instagram.com/");
                
                response = _restClient.Execute(new RestRequest(redirectAddr));
    
                if (response.Content.Contains("<form action=\"/oauth/authorize"))
                {
                    var angle = _htmlParser.Parse(response.Content);
    
                    var form = angle.QuerySelector("form");
    
                    var actionUrl = HttpUtility.UrlDecode(form.GetAttribute("action"));
                    var token = angle.QuerySelector("[name=csrfmiddlewaretoken]").GetAttribute("value");
    
                    response = _restClient.Execute(new RestRequest(actionUrl, Method.POST).
                        AddParameter("csrfmiddlewaretoken", token).
                        AddParameter("allow", "Authorize").
                        AddHeader("Referer", "https://www.instagram.com"+actionUrl));
                    
                    redirectAddr = (string) response.Headers.First(x => x.Name.Equals("Location")).Value.ToString().Replace("https://www.instagram.com", "");
                    redirectAddr = redirectAddr.Replace("https://ulogin.ru", "");
                    
                    if (redirectAddr.Contains("denied"))
                    {
                        return false;
                    }
                }
              
                _restClient.BaseUrl = new Uri("https://ulogin.ru/");
                
                redirectAddr = (string) response.Headers.First(x => x.Name.Equals("Location")).Value.ToString().Replace("https://www.instagram.com", "");
                redirectAddr = redirectAddr.Replace("https://ulogin.ru", "");
                
                response = _restClient.Execute(new RestRequest(redirectAddr));
                    
                redirectAddr = (string) response.Headers.First(x => x.Name.Equals("Location")).Value;
               
                var dict = redirectAddr.Replace("http://ulogin.ru/post_redirect.html?", "").Split('&').Select(x => HttpUtility.UrlDecode(x).Split('=')).ToDictionary(x => x[0], y => y[1]);
    
                if (dict.ContainsKey("redirect_uri"))
                {
                    _restClient.BaseUrl = new Uri(dict["redirect_uri"]);
    
                    response = _restClient.Execute(new RestRequest("/", Method.POST).AddParameter("token", dict["token"]));
                    
                    _restClient.BaseUrl = new Uri("http://socgain.com/");
    
                    _restClient.FollowRedirects = true;
    
                    response = _restClient.Execute(new RestRequest(response.Content));
    
                    if (!response.Content.Contains("class=\"hello-bar\""))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}