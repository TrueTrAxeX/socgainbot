﻿using System;
using System.Net;
using System.Web.UI.WebControls;

namespace SocgainBot.Core.Database.Model
{
    public enum AccountType
    {
        Instagram, Vkontakte
    }

    public class ProxyData
    {
        public Uri Address { get; set; }
        public string Login { get; set; } = null;
        public string Password { get; set; } = null;
    }
    
    public class UserAccount
    {
        public AccountType AccountType { get; set; }
        
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        public ProxyData ProxyData { get; set; }
        
        public bool Enabled { get; set; }

        public string BrowserUserAgent { get; set; } = null;

        public int DelayAfterLikeTask { get; set; } = 5000;
        public int DelayAfterSubscribeTask { get; set; } = 60000;
        
        public int TotalLikesInDay { get; set; } = 0;
        public int TotalSubscribesInDay { get; set; } = 0;
        
        public DateTime LastLikeTime { get; set; }
        public DateTime LastSubscribeTime { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is UserAccount ua)
            {
                if (ua.Id.Equals(this.Id))
                {
                    return true;
                }
            }

            return false;
        }
    }
}