﻿using LiteDB;
using SocgainBot.Core.Database.Model;

namespace SocgainBot.Core.Database
{
    public class DatabaseManager
    {
        private static LiteDatabase Db = new LiteDatabase("app.db");

        public static LiteCollection<UserAccount> UserAccountCollection = Db.GetCollection<UserAccount>();
    }
}