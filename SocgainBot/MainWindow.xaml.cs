﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;
using System.Windows;
using System.Windows.Controls;
using SocgainBot.Core;
using SocgainBot.Core.Bot.Instagram;
using SocgainBot.Core.Database;
using SocgainBot.Core.Database.Model;
using SocgainBot.Core.Settings;
using SocgainBot.Core.Utils;
using TheArtOfDev.HtmlRenderer.WPF;

namespace SocgainBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            
            InstagramLoadUiElements();

            void InstEnableLikeCheckboxChanged(bool chckd)
            {
                InstagramSettings.EnableLike = chckd;
            }

            void InstEnableSubscribeCheckboxChanged(bool chckd)
            {
                InstagramSettings.EnableSubscribe = chckd;
            }
            
            InstEnableLikeCheckbox.Checked += (sender, args) => { InstEnableLikeCheckboxChanged(true); };
            InstEnableLikeCheckbox.Unchecked += (sender, args) => { InstEnableLikeCheckboxChanged(false); };
            InstEnableSubscribeCheckbox.Checked += (sender, args) => { InstEnableSubscribeCheckboxChanged(true); };
            InstEnableSubscribeCheckbox.Unchecked += (sender, args) => { InstEnableSubscribeCheckboxChanged(false); };

            InstThreadCountComboBox.SelectionChanged += (sender, args) =>
            {
                string tCount = ((ComboBoxItem) InstThreadCountComboBox.SelectedItem).Content.ToString();

                InstagramSettings.BotThreadCount = int.Parse(tCount);
            };
            
            InstUserAgentComboBox.SelectionChanged += (sender, args) =>
            {
                string tt = ((ComboBoxItem) InstUserAgentComboBox.SelectedItem).Content.ToString();
                
                InstagramSettings.DefaultUserAgent = tt;
            };

            InstLogBox.MouseDoubleClick += (sender, args) =>
            {
                if (InstLogBox.SelectedItem != null)
                {
                    var item = (ListBoxItem) InstLogBox.SelectedItem;

                    Clipboard.SetText(((HtmlLabel)item.Content).Text);
                }
            };

            MaxLikesInDayTextBox.TextChanged += (sender, args) => {
                try
                {
                    int count = int.Parse(MaxLikesInDayTextBox.Text);
                    InstagramSettings.MaxLikesInDay = count;
                }
                catch
                {
                    
                } 
            };

            MaxSubscribesInDayTextBox.TextChanged += (sender, args) => {
                try
                {
                    int count = int.Parse(MaxSubscribesInDayTextBox.Text);
                    InstagramSettings.MaxSubscribesInDay = count;
                }
                catch
                {
                    
                } 
            };
            
            this.Closed += (sender, args) =>
            {
                Environment.Exit(1);
            };
        }

        private void InstagramLoadUiElements()
        {
            var data = DatabaseManager.UserAccountCollection.Find(x => x.AccountType == AccountType.Instagram)
                .OrderBy(x => x.Login);
            
            data.Select(x => BuildAccountListBoxItem(x)).ToList().ForEach(x => { InstAccountListBox.Items.Add(x); });

            InstEnableLikeCheckbox.IsChecked = InstagramSettings.EnableLike;
            InstEnableSubscribeCheckbox.IsChecked = InstagramSettings.EnableSubscribe;
            
            foreach (ComboBoxItem item in InstThreadCountComboBox.Items)
            {
                if(item.Content.ToString().Equals(InstagramSettings.BotThreadCount.ToString()))
                {
                    InstThreadCountComboBox.SelectedItem = item;
                    break;
                }
            }

            foreach (var item in UserAgents.List)
            {
                InstUserAgentComboBox.Items.Add(new ComboBoxItem()
                {
                    Tag = item.Value,
                    Content = item.Key
                });
            }

            if (InstagramSettings.DefaultUserAgent != null)
            {
                foreach (ComboBoxItem item in InstUserAgentComboBox.Items)
                {
                    if (InstagramSettings.DefaultUserAgent.Equals(item.Content.ToString()))
                    {
                        InstUserAgentComboBox.SelectedItem = item;
                        break;
                    }
                }
            }

            MaxLikesInDayTextBox.Text = InstagramSettings.MaxLikesInDay.ToString();
            MaxSubscribesInDayTextBox.Text = InstagramSettings.MaxSubscribesInDay.ToString();
            
        }

        private void InstAddAccountBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var login = InstLogin.Text;
            var password = InstPassword.Password;

            if (login.Length > 30 || password.Length > 30)
            {
                MessageBox.Show("Слишком длинный логин или пароль!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Пароль или логин не могут быть пустыми", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            if (DatabaseManager.UserAccountCollection.Exists(x =>
                x.AccountType == AccountType.Instagram && x.Login.Equals(login)))
            {
                MessageBox.Show("Такой аккаунт уже есть! Удалите старый аккаунт, чтобы добавить новый", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            
            UserAccount account = new UserAccount()
            {
                AccountType = AccountType.Instagram,
                Enabled = true,
                Login = login,
                Password = password
            };
            
            DatabaseManager.UserAccountCollection.Insert(account);
            DatabaseManager.UserAccountCollection.EnsureIndex(x => x.Login);
            
            InstAccountListBox.Items.Add(BuildAccountListBoxItem(account));

            InstLogin.Text = "";
            InstPassword.Password = "";
            
            MessageBox.Show("Аккаунт успешно добавлен!", "Успех", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private ListBoxItem BuildAccountListBoxItem(UserAccount account)
        {
            void EnableDisableAllOrderButton(bool isEnabled)
            {
                foreach (ListBoxItem item in InstAccountListBox.Items)
                {
                    var s = (StackPanel) item.Content;

                    foreach (var c in s.Children)
                    {
                        if (c is Button b)
                        {
                            if (b.Name.Equals("OrderBtn"))
                            {
                                b.IsEnabled = isEnabled;
                            }
                        }
                    }
                }
            }
            
            ListBoxItem listBoxItem = new ListBoxItem();
            listBoxItem.Tag = account.Id.ToString();
            
            StackPanel sp = new StackPanel();
            
            sp.Orientation = Orientation.Horizontal;

            sp.Children.Add(new TextBlock()
            {
                Margin = new Thickness(5),
                MinWidth = 250,
                Text = "Аккаунт: " + account.Login
            });

            var enabledCheckbox = new CheckBox()
            {
                IsChecked = account.Enabled,
                Content = "Вкл.",
                Margin = new Thickness(5),
                Name = "Enabled"
            };

            enabledCheckbox.Checked += (sender, args) =>
                {
                    var acc = DatabaseManager.UserAccountCollection.Find(x => x.Id.Equals(account.Id)).FirstOrDefault();

                    if (acc != null)
                    {
                        acc.Enabled = true;
                        DatabaseManager.UserAccountCollection.Update(acc);
                    }
                };

            enabledCheckbox.Unchecked += (sender, args) =>
            {
                var acc = DatabaseManager.UserAccountCollection.Find(x => x.Id.Equals(account.Id)).FirstOrDefault();

                if (acc != null)
                {
                    acc.Enabled = false;
                    DatabaseManager.UserAccountCollection.Update(acc);
                }
            };
            
            sp.Children.Add(enabledCheckbox);

            var settingsButton = new Button()
            {
                Content = "Настройки",
                Margin = new Thickness(5),
                Name = "SettingsBtn"
            };

            settingsButton.Click += (sender, args) =>
            {
                var w = new AccountSettingsWindow(account);
                w.Title = account.Login + " - Настройка аккаунта";
                w.Owner = this;
                w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                w.ShowDialog();
            };
            
            var orderButton = new Button()
            {
                Content = "Заказ",
                Margin = new Thickness(5),
                Name = "OrderBtn"
            };

            orderButton.Click += async (sender, args) =>
            {
                EnableDisableAllOrderButton(false);

                bool authenticated = false;
                int? balance = 0;
                
                string userAgent = null;

                userAgent = UserAgents.List[InstagramSettings.DefaultUserAgent];

                if (!string.IsNullOrEmpty(account.BrowserUserAgent))
                {
                    userAgent = account.BrowserUserAgent;
                }
                
                InstagramBot bot = new InstagramBot();
                InstagramSocgainBot sbot = new InstagramSocgainBot(bot);

                sbot.ProxyData = account.ProxyData;
                bot.ProxyData = account.ProxyData;
                bot.UserAgent = userAgent;
                sbot.UserAgent = userAgent;

                await Task.Factory.StartNew(() =>
                {
                    if (bot.Auth(new InstagramBot.InstagramCredentials() {Login = account.Login, Password = account.Password}, true))
                    {
                        if (sbot.Auth())
                        {
                            authenticated = true;

                            balance = sbot.GetBalance();
                        }
                    }
                });

                EnableDisableAllOrderButton(true);

                if (authenticated)
                {
                    var w = new InstagramOrderWindow(account, bot, sbot, (balance ?? 0));
                    w.Title = account.Login + " - Заказ накрутки";
                    w.Owner = this;
                    w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    w.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Не удалось авторизоваться на сервисе socgain.com");
                }
            };

            sp.Children.Add(orderButton);
            
            sp.Children.Add(settingsButton);

            var removeButton = new Button()
            {
                Content = "Удалить",
                Margin = new Thickness(5),
                Name = "RemoveBtn",
                Tag = account.Id
            };

            removeButton.Click += (sender, args) =>
            {
                DatabaseManager.UserAccountCollection.Delete(x => x.Id.Equals(account.Id));
                InstAccountListBox.Items.Remove(listBoxItem);
            };
            
            sp.Children.Add(removeButton);

            listBoxItem.Content = sp;

            return listBoxItem;
        }

        private bool IsInstBotRunning { get; set; } = false;
        
        private async void InstBotRunBtn_OnClick(object sender, RoutedEventArgs e)
        {
            IsInstBotRunning = !IsInstBotRunning;

            if (IsInstBotRunning)
            {
                InstBotRunBtn.Content = "Остановить бота";
                InstagramBotWorker.Start(this);
            }
            else
            {
                InstBotRunBtn.Content = "Останавливаем...";
                InstBotRunBtn.IsEnabled = false;
                
                var t = Task.Factory.StartNew(() =>
                {
                    InstagramBotWorker.Stop();
                    Thread.Sleep(3000);
                });

                await t;
                
                InstBotRunBtn.IsEnabled = true;
                InstBotRunBtn.Content = "Запустить бота";
            }
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            foreach (var keyValuePair in InstagramBotWorker._botDictionary)
            {
                keyValuePair.Value.InstagramBot.TerminateBrowser();
            }
        }
    }
}